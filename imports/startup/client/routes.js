import {FlowRouter} from 'meteor/kadira:flow-router'
import {BlazeLayout} from 'meteor/kadira:blaze-layout'
import bpmns from '../../ui/bpmn.js'

FlowRouter.route('/:id',{
action:function(params){
 console.log(params)
  BlazeLayout.render('applicationLayout',{main:'bpmns', workflowId:params.id});
}
})
