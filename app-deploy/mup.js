module.exports = {
  servers: {
    one: {
      // TODO: set host address, username, and authentication method
      host: '13.250.11.181',
      username: 'ubuntu',
      pem: '../../../../aptitude_v3.pem'
      // password: 'server-password' or neither for authenticate from ssh-agent
    }
  },

  meteor: {
    // TODO: change app name and path
    name: 'aptitude-workflow',
    path: '../../aptitude-workflow',

    servers: {
      one: {}
    },

    buildOptions: {
      serverOnly: true
    },

    env: {
      // TODO: Change to your app's url If you are using ssl, it needs to start with
      // https:
      PORT: 8082,
      ROOT_URL: 'http://aptitude-workflow.com',
      MONGO_URL: 'mongodb://127.0.0.1:27017/aptitude'
    },

    // ssl: { // (optional)   // Enables let's encrypt (optional)   autogenerate: {
    // email: 'email.address@domain.com',     // comma seperated list of domains
    // domains: 'website.com,www.website.com'   } },

    docker: {
      // change to 'kadirahq/meteord' if your app is not using Meteor 1.4
      image: 'abernix/meteord:base',
      imagePort: 8082,
      args: ['--link=mongodb:mongodb'] // (default: 80, some
      // images EXPOSE different ports)
    },

    // This is the maximum time in seconds it will wait for your app to start Add 30
    // seconds if the server has 512mb of ram And 30 more if you have binary npm
    // dependencies.
    deployCheckWaitTime: 60,
    // deployCheckPort: 30, Show progress bar while uploading bundle to server You
    // might need to disable it on CI servers
    enableUploadProgressBar: true
  },

  // mongo: {   port: 27017,   version: '3.4.1',   servers: {     one: {}   } }
};