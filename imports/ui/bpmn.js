import {Template} from 'meteor/templating';
import {ReactiveVar} from 'meteor/reactive-var';
BpmnModeler = require('bpmn-js/lib/Modeler');
import './bpmn.html'
var convert = require('xml-js');

const newDiagramXML = '<?xml version="1.0" encoding="UTF-8"?><bpmn2:definitions xmlns:xsi="http://www.w' +
    '3.org/2001/XMLSchema-instance" xmlns:bpmn2="http://www.omg.org/spec/BPMN/2010052' +
    '4/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http:' +
    '//www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524' +
    '/DI" xsi:schemaLocation="http://www.omg.org/spec/BPMN/20100524/MODEL BPMN20.xsd"' +
    ' id="sample-diagram" targetNamespace="http://bpmn.io/schema/bpmn"><bpmn2:process' +
    ' id="Process_1" isExecutable="false"><bpmn2:startEvent id="StartEvent_1"/></bpmn' +
    '2:process><bpmndi:BPMNDiagram id="BPMNDiagram_1"><bpmndi:BPMNPlane id="BPMNPlane' +
    '_1" bpmnElement="Process_1"><bpmndi:BPMNShape id="_BPMNShape_StartEvent_2" bpmnE' +
    'lement="StartEvent_1"><dc:Bounds height="36.0" width="36.0" x="412.0" y="240.0"/' +
    '></bpmndi:BPMNShape></bpmndi:BPMNPlane></bpmndi:BPMNDiagram></bpmn2:definitions>';

Template
  .bpmns
  .onCreated(function () {
    console.log(this.data.workflowId())
    var self = this;
    // counter starts at 0
    this.task = new ReactiveVar(false);
    this.page = new ReactiveVar(false)
    this.to = new ReactiveVar([])
    this.cc = new ReactiveVar([])
    this.bcc = new ReactiveVar([])
    this.flowchartData = new ReactiveVar('');
    this.emailDetail = new ReactiveVar('');
    this.loadWorkflow = new ReactiveVar(false);
    this.loadPage = new ReactiveVar(false);
    this.flowchart = new ReactiveVar({});
    this.sendCount = new ReactiveVar(0);
    this.targetPage = new ReactiveVar('');
    this.conditional = new ReactiveVar(false);
    this.showRoles = new ReactiveVar(false);
    this.taskId = new ReactiveVar('');
    this.emails = new ReactiveVar([]);
    this.jsonform = new ReactiveVar('');
    Meteor.call('getWorkFlow', function (err, res) {
      workflow = _.findWhere(res, {
        _id: self
          .data
          .workflowId()
      });

      self.workflow = new ReactiveVar(workflow);

      self
        .loadWorkflow
        .set(true);
    })

    Meteor.call('getPage', function (err, res) {
      self.pages = new ReactiveVar(res);

      console.log(self.pages);
      self
        .loadPage
        .set(true);
    })
    //alert('helo')
  });

Template
  .bpmns
  .onRendered(function () {

    document
      .addEventListener("click", function (e) {
        var eve = $(e.target).text();
        if (eve.search('Manual') > -1) 
          $('#manual').trigger('click');
        if (eve.search('Service') > -1) 
          $('#assigner').trigger('click');
        if (eve.search('User') > -1) 
          $('#pages_btn').trigger('click');
        }
      );

    var self = this;
    var container = $('#js-drop-zone');

    var canvas = $('#js-canvas');

    var modeler = new BpmnModeler({container: canvas});

    // var newDiagramXML = fs.readFileSync(__dirname +
    // '/../resources/newDiagram.bpmn', 'utf-8');

    function createNewDiagram() {
      openDiagram(newDiagramXML);
    }

    function openDiagram(xml) {
      modeler
        .importXML(xml, function (err) {
          if (err) {
            container
              .removeClass('with-diagram')
              .addClass('with-error');

            container
              .find('.error pre')
              .text(err.message);
          } else {
            container
              .removeClass('with-error')
              .addClass('with-diagram');
          }

        });
    }

    function saveSVG(done) {
      modeler.saveSVG(done);
    }

    function saveDiagram(done) {
      //console.log(modeler)
      modeler
        .saveXML({
          format: true
        }, function (err, xml) {
          var jsonform = convert.xml2json(xml, {
            compact: true,
            spaces: 4
          })
          var form = JSON.parse(jsonform);
          self
            .jsonform
            .set(form);

          var user = form['bpmn2:definitions']['bpmn2:process']['bpmn2:userTask'];
          var send = form['bpmn2:definitions']['bpmn2:process']['bpmn2:sendTask'];
          var manual = form['bpmn2:definitions']['bpmn2:process']['bpmn2:manualTask'];

          var flowchart = self
            .flowchart
            .get();

          //console.log(_.size(self.flowchart.get()))

          if (user === undefined && _.size(self.flowchart.get()) === 1) {

            self
              .flowchart
              .set({});
          }
          console.log(user);

          if (user !== undefined) {
            var task = _.omit(self.flowchart.get(), 'action', 'approve');
            console.log(task);
            console.log(self.flowchart.get());
            if (user.constructor !== Array && _.size(task) === 2) {
              self
                .flowchart
                .set(_.omit(self.flowchart.get(), user._attributes.id));

            } else if (user.constructor === Array && user.length < _.size(self.flowchart.get())) {
              var keys = _.keys(self.flowchart.get());
              _.each(keys, function (s) {
                var c = 0;
                _.each(user, function (single) {
                  single._attributes.id === s
                    ? c = c + 1
                    : '';
                });
                if (c == 0 && s.search('Task') > -1) {
                  self
                    .flowchart
                    .set(_.omit(self.flowchart.get(), s))
                }

              });
              console.log(self.flowchart.get());

            }
          }

          // if (user) {   var l = user.constructor === Array && user.length >
          // _.size(flowchart);   console.log(flowchart);   console.log(l,
          // _.size(flowchart));   if (_.size(flowchart) === 0 || l) {
          // $('#pages_btn').trigger('click');   } }

          var c = self
            .sendCount
            .get();

          if (send) {
            if (user) {
              var m = send.constructor === Array && send.length > c;
              var n = m
                ? ''
                : send.constructor !== Array && _.isObject(send) && c == 0
              if (n || m) {
                //  var s=q[q.length-1];
                var a = $('.djs-group .selected').attr('data-element-id');
                var b = JSON.parse(jsonform)['bpmn2:definitions']['bpmn2:process']['bpmn2:sendTask'];
                var s = b.constructor === Array
                  ? _.find(b, function (single) {
                    return single._attributes.id === a
                  })
                  : b;

                var incom = s['bpmn2:incoming']['_text'];
                var arrow = _.find(JSON.parse(jsonform)['bpmn2:definitions']['bpmn2:process']['bpmn2:sequenceFlow'], function (single) {
                  return single._attributes.id === incom;
                });

                arrow
                  ._attributes
                  .sourceRef
                  .search('ExclusiveGateway') > -1
                  ? self
                    .conditional
                    .set(true)
                  : self
                    .conditional
                    .set(false);

                //var k=_.isObject(user) ? user._attributes.id :;

                if (user.constructor === Array) {
                  var p = user.length - 1;
                  var t = user[p]['_attributes']['id'];
                  self
                    .targetPage
                    .set(t);

                } else {
                  self
                    .targetPage
                    .set(user._attributes.id);

                }

                self
                  .taskId
                  .set(s._attributes.id);

                $('#email').trigger('click');
                c++;
                self
                  .sendCount
                  .set(c);
              }
            } else {
              if (send) {
                var sendC = send.constructor !== Array && _.isObject(send)
                  ? 1
                  : send.length;

                if (sendC !== self.emails.get().length) 
                  $('#email').trigger('click');
                }
              }

          }

          self
            .flowchartData
            .set(JSON.parse(jsonform));

          done(err, xml);
        });
    }

    function registerFileDrop(container, callback) {

      function handleFileSelect(e) {
        e.stopPropagation();
        e.preventDefault();

        var files = e.dataTransfer.files;
        var file = files[0];
        var reader = new FileReader();
        reader.onload = function (e) {
          var xml = e.target.result;
          callback(xml);
        };

        reader.readAsText(file);
      }

      function handleDragOver(e) {
        e.stopPropagation();
        e.preventDefault();

        e.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
      }

      container
        .get(0)
        .addEventListener('dragover', handleDragOver, false);
      container
        .get(0)
        .addEventListener('drop', handleFileSelect, false);
    }

    ////// file drag / drop /////////////////////// check file api availability
    if (!window.FileList || !window.FileReader) {
      window.alert('Looks like you use an older browser that does not support drag and drop. Try usi' +
          'ng Chrome, Firefox or the Internet Explorer > 10.');
    } else {
      registerFileDrop(container, openDiagram);
    }

    // bootstrap diagram functions

    $('#js-create-diagram')
      .click(function (e) {
        e.stopPropagation();
        e.preventDefault();
        createNewDiagram();
      });

    var downloadLink = $('#js-download-diagram');
    var downloadSvgLink = $('#js-download-svg');

    $('.buttons a').click(function (e) {
      if (!$(this).is('.active')) {
        e.preventDefault();
        e.stopPropagation();
      }
    });

    function setEncoded(link, name, data) {
      var encodedData = encodeURIComponent(data);
      if (data) {
        link
          .addClass('active')
          .attr({
            'href': 'data:application/bpmn20-xml;charset=UTF-8,' + encodedData,
            'download': name
          });
      } else {
        link.removeClass('active');
      }
    }

    var _ = require('lodash');

    var exportArtifacts = _.debounce(function () {

      saveSVG(function (err, svg) {
        setEncoded(downloadSvgLink, 'diagram.svg', err
          ? null
          : svg);
      });

      saveDiagram(function (err, xml) {
        setEncoded(downloadLink, 'diagram.bpmn', err
          ? null
          : xml);
      });
    }, 500);

    modeler.on('commandStack.changed', exportArtifacts);

  })

Template
  .bpmns
  .helpers({
    assignerRole() {
      return Template
        .instance()
        .workflow
        .get()
        .roles;
    },
    counter() {
      return Template
        .instance()
        .counter
        .get();
    },

    cc() {
      return Template
        .instance()
        .cc
        .get();
    },
    bcc() {
      return Template
        .instance()
        .bcc
        .get();
    },
    to() {
      return Template
        .instance()
        .to
        .get();
    },
    pages() {
      var tem = Template.instance();
      if (tem.loadWorkflow.get() && tem.loadPage.get()) {

        return _.where(tem.pages.get(), {
          user: tem
            .workflow
            .get()
            .user
        })
      }
    },
    conditional() {
      return Template
        .instance()
        .conditional
        .get();
    },
    showRoles() {
      return Template
        .instance()
        .showRoles
        .get()
    },

    pageFields() {
      var t = Template.instance();
      var page = t
        .flowchart
        .get()[
          t
            .targetPage
            .get()
        ]
        .page
        .form
        .form;

      var arr = JSON.parse(JSON.parse(page));
      var fields = _.filter(arr, function (single) {
        return single
          .name
          .search('radio') > -1 || single
          .name
          .search('select') > -1 || single
          .name
          .search('checkbox') > -1
      })
      return fields;
    },
    workflowRoles() {
      return Template
        .instance()
        .workflow
        .get()
        .roles;

    },
    loadWorkflow() {
      return Template
        .instance()
        .loadWorkflow
        .get();
    }
  });

//events for bpmn template
Template
  .bpmns
  .events({

    'click input[name="assigner"]' (event, instance) {
      let roles = [];
      _.each($('input[name="assigner"]:checked'), (single) => {
        roles.push($(single).val());

      });

      let flowchart = instance
        .flowchart
        .get();

      let keys = _.keys(flowchart);
      let lastkey = keys[keys.length - 1] == "assignerRoles"
        ? keys[keys.length - 2]
        : keys[keys.length - 1];

      let assignerRole = {
        page: flowchart[lastkey]['page']['_id'],
        roles: roles
      };

      if (flowchart.assignerRoles) {

        let ass = _.findWhere(flowchart.assignerRoles, {page: flowchart[lastkey]['page']['_id']
        });

        ass
          ? ass.roles = _.union(ass.roles, roles)
          : flowchart
            .assignerRoles
            .push(assignerRole);
      } else {
        flowchart.assignerRoles = [assignerRole];

      }

      instance
        .flowchart
        .set(flowchart);
      console.log(instance.flowchart.get());

    },

    'keyup #cc' (event, instance) {
      if (event.keyCode == 13) {
        let tem = instance
          .cc
          .get()
        tem.push({value: event.target.value});
        instance
          .cc
          .set(tem);
        event.target.value = '';
        console.log(instance.cc.get())

      }
    },
    'keyup #bcc' (event, instance) {
      if (event.keyCode == 13) {
        let tem = instance
          .bcc
          .get()
        tem.push({value: event.target.value});
        instance
          .bcc
          .set(tem);
        event.target.value = ''
      }
    },

    'click #role' (e, t) {
      var role = []
      _.each($("input[name='role']:checked"), function (single) {
        role.push(single.value)
      })
      t
        .flowchart
        .get()[
          t
            .targetPage
            .get()
        ]['roles'] = role;

    },
    'click #saveDetail' (event, t) {
      var sub = $("#subject").val()
      var body = $("#body").val()
      var form = t
        .jsonform
        .get()
      var user = form['bpmn2:definitions']['bpmn2:process']['bpmn2:userTask'];
      var send = form['bpmn2:definitions']['bpmn2:process']['bpmn2:sendTask'];

      if (!user && send) {
        var emails = t
          .emails
          .get();
        emails.push({
          subjec: sub,
          body: body,
          to: $("#to").val(),
          cc: t
            .cc
            .get(),
          bcc: t
            .bcc
            .get()
        })
        t
          .emails
          .set(emails)
      } else {
        var c;
        if (t.conditional.get()) {
          var conditional = $("input[name='pageField']:checked").val()
          var field = $("input[name='pageField']:checked")
            .parent()
            .parent()
            .parent()
            .parent()
            .attr('id');
          c = {
            parent: field,
            condition: conditional
          }
        }

        var emailDetail = {
          subjec: sub,
          body: body,
          to: $("#to").val(),
          cc: t
            .cc
            .get(),
          bcc: t
            .bcc
            .get()
        }
        c
          ? emailDetail.conditional = c
          : '';
        var tem = t
          .flowchart
          .get();
        var news;
        var e = tem[
          t
            .targetPage
            .get()
        ]['emailDetail'];

        if (e === undefined) {
          news = emailDetail
          tem[
            t
              .targetPage
              .get()
          ]['emailDetail'] = [news];
        } else {
          news = tem[
            t
              .targetPage
              .get()
          ]['emailDetail'];
          news.push(emailDetail);
        }
        t
          .flowchart
          .set(tem);
        console.log(t.flowchart.get())

      }
      $("#subject").val("")
      $("#body").val("")
      $("#to").val("")
      t
        .cc
        .set([])
      t
        .bcc
        .set([]);
    },

    'click #save' (event, instance) {
      console.log(instance.flowchart.get());
      var charts = {}
      _.each($("input[name='charts']:checked"), (single) => {
        charts[single.value] = true;
      });

      var details = instance
        .flowchart
        .get();
      var f = instance
        .flowchartData
        .get();

      var arr = f['bpmn2:definitions']['bpmn2:process']['bpmn2:userTask'];
      if (arr) {
        if (arr.constructor === Array) {
          _
            .each(f['bpmn2:definitions']['bpmn2:process']['bpmn2:userTask'], function (single) {
              single.detail = details[single._attributes.id];
              console.log(details[single._attributes.id]);
            });

        } else {
          arr.detail = details[arr._attributes.id];
          f['bpmn2:definitions']['bpmn2:process']['bpmn2:userTask'] = [arr]
          console.log(arr._attributes.id);
        }

      }

      var t = f['bpmn2:definitions']['bpmn2:process']['bpmn2:userTask'];
      console.log(t);
      var data = {
        charts: charts,
        workflow: instance
          .data
          .workflowId(),
        flowchartData: f,
        emails: instance
          .emails
          .get(),
        totalLvl: t.length

      };

      details.approve
        ? data.approve = details.approve
        : '';

      details.assignerRoles
        ? data.assignerRoles = details.assignerRoles
        : "";

      details.action
        ? data.action = details.action
        : '';

      Meteor.call("saveFlowchart", data);
      $('#save').prop("disabled", true);
    },

    'click #page' (e, t) {
      var tem = t
        .flowchart
        .get();

      var page = _.findWhere(t.pages.get(), {_id: e.target.value});
      var nod = $('.djs-group .selected').attr('data-element-id');
      console.log(nod);
      console.log(tem);
      t
        .targetPage
        .set(nod);

      tem[nod] == undefined
        ? tem[nod] = {}
        : '';

      tem[nod]['page'] = page;
      t
        .showRoles
        .set(true)
      console.log(t.targetPage.get());
      console.log(t.flowchart.get());
    },

    'click #action' (e, t) {
      var role = _.map($('input[name="actionRole"]:checked'), (single) => {
        return single.value;
      });
      console.log(role);
      var action = $('input[name="action"]:checked').val();

      var flowchart = t
        .flowchart
        .get();

      if (action === 'approve') {

        flowchart.approve
          ? flowchart
            .approve
            .push({status: "open", roles: role})
          : flowchart.approve = [
            {
              status: "open",
              roles: role
            }
          ];
        t
          .flowchart
          .set(flowchart)

      } else {
        flowchart.action = {
          name: action,
          roles: role
        };
        t
          .flowchart
          .set(flowchart)
      }

      $('input[name="action"]:checked').prop('checked', false);
      $('input[name="actionRole"]:checked').prop('checked', false);
      console.log(t.flowchart.get());
    }

  });
