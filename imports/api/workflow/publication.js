import {SampleData} from './collection/sampleData.collection.js';
import {WorkflowDb} from './collection/workflow.collection.js';

Meteor.publish('getWorkFlow',function(){
     if(this.userId){
       return WorkflowDb.find();
  }
  else{
    throw new Meteor.Error('list.unauthorized',
      'This list doesn\'t belong to you.');
  }
 
})

Meteor.publish('getSampleData',function(){
  if(this.userId){
       return SampleData.find({});
  }
  else{
    throw new Meteor.Error('list.unauthorized',
      'This list doesn\'t belong to you.');
  }
 
})
