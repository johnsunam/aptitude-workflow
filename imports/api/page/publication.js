import {PageDb} from './collection/page.collection.js'

Meteor.publish('getPage',function(){
   if(this.userId){
    return PageDb.find();
  }
  else{
    throw new Meteor.Error('list.unauthorized',
      'This list doesn\'t belong to you.');
  }
  
})